package ru.tsc.korosteleva.tm.api.repository;

import ru.tsc.korosteleva.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    Task add(Task task);

    Task create(String name);

    Task create(String name, String description);

    List<Task> findAll();

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    Task findOneByName(String name);

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

    Task remove(Task task);

    Task removeById(String id);

    Task removeByIndex(Integer index);

    Task removeByName(String name);

    void clear();

}