package ru.tsc.korosteleva.tm.api.controller;

public interface ITaskController {

    void createTask();

    void showTaskList();

    void clearTasks();

    void showTaskById();

    void showTaskByIndex();

    void updateTaskById();

    void updateTaskByIndex();

    void removeTaskById();

    void removeTaskByIndex();

    void removeTaskByName();

}
